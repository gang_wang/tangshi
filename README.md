## An Online Game about Classic Chinese Poems ##
![screen shot](public/tang-shi-screen-shot.png)
### What ###
I have always enjoyed classic Chinese literature, particularly the classic poems. So I wrote this game for my own entertainment. 

The rules of game are very simple: 

 * You will be presented with a list of (either 5 or 7) sentences, each from a poem included a database that contains 10,000+ of them. 
 * These sentences are *not* randomly picked. Actually, you can form another sentence (in a different poem) by using one character from each of the sentences provided. 
 * Your job is **to identify that new poem sentence embedded**. 

### How to use ###
The code is written in *Ruby on Rails*, with a bit *jQuery* JavaScript for UI, while a *SQLite* is used as the database. It has been tested using Ruby 2.3 on Windows, Mac and Ubuntu Linux. 

To run the system, make sure you have `ruby` installed. 
 
 * Git clone this repo. 
 * Run `bundle install` in the repo's root directory. 
 * Run `rails s -p #{your_port_number}`. That's it. 

--- Gang Wang
