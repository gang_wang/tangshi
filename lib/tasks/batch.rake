namespace :util do

  def get_solutions(str, pid=nil)
    solutions = {}
    str.each_char do |zi|
      solutions[zi] = []
      poems = Poem.find(:all, :conditions=>['content like ?', "%#{zi}%"])
      return nil if poems.size == 0
      poems.each do |p|
        next if pid != nil and p.id == pid
        s, c = p.get_matched_segment(zi)
        next if s.mb_chars.length != 5 and s.mb_chars.length != 7
        solutions[zi] << [p.id, c, s.mb_chars.length]
      end
      return nil if solutions[zi].size == 0
    end
    solutions
  end

  desc 'comb through all the poems and filter out the good ones.'
  task :batch => :environment  do |t|
    Poem.find(:all).each do |p|
      next if p.id == 356
      puts "Processing poem #{p.id} ..."
      count = 0
      p.get_segments.each do |s|
        count += 1
        next if s.mb_chars.length != 5 and s.mb_chars.length != 7
        ret = get_solutions(s, p.id)
        next if ret.nil?
        sol = Solution.new
        sol.name    = s
        sol.poem_id = p.id
        sol.segment = count - 1
        sol.data    = ret
        sol.zishu   = s.mb_chars.length
        sol.save
        puts "Saved solutions for #{p.id}-#{count} ..."
      end
    end
  end

end
