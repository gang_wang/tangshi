class MainController < ApplicationController

  def index
    @poem = Poem.find(params[:id])
    respond_to do |format|
      format.js
      format.html
    end
  end

  def author
    #@poems = Poem.find(:all, :conditions => ['author = ?', params[:id]])
    @poems = Poem.where('author=?', params[:id])
  end

  def search
    @solutions = {}
    params[:id].each_char do |zi|
      # @solutions[zi] = Poem.find(:all, :conditions=>['content like ?', "%#{zi}%"])
      @solutions[zi] = Poem.where('content like ?', "%#{zi}%")
    end
  end

  def target
    if not params[:id].nil?
      # @target = Solution.find(:first, :conditions => ['name=?', params[:id]])
      # @target = Solution.find_by name: params[:id]
      # @target = Solution.where('name=?', params[:id]).take
      @target = Solution.where('name=?', params[:id])[0]
      @len_constraint = [5,7].sample
    end
    respond_to do |format|
      format.js
      format.html { redirect_to :action=>'game', :id=>@target.id }
    end
  end

  def game
    if params[:id].nil?
      while (1)
        #@target = Solution.find(id = (1..Solution.maximum("id")).to_a.sample)
        #@target = Solution.find(id = Solution.select('id').where('zishu = ?', 5).sample)
        @target = Solution.find(id = Solution.select('id').where('zishu > ?', 0).sample)
        # check
        @len_constraint = [5,7].sample
        break if @target.is_length_ok?(@len_constraint)
        @len_constraint = (5+7) - @len_constraint
        break if @target.is_length_ok?(@len_constraint)
      end
    else
      @target = Solution.find(id=params[:id])
      @len_constraint = [5,7].sample
    end
    respond_to do |format|
      format.js
      format.html
    end
  end

  def reveal
    respond_to do |format|
      format.js
    end
  end

  def show
    @poem = Poem.find(id=params[:id])
    respond_to do |format|
      format.js
    end
  end

end
