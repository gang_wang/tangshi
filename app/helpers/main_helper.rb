module MainHelper

  def format_poem_line(line, zi)
    i = line.mb_chars.index(zi)
    left  = ( i == 0) ? '' : line.mb_chars[0..i-1]
    right = ( i == line.mb_chars.length - 1) ? '' : line.mb_chars[i+1..-1]
    "#{left}<span class=\"answer\">#{zi}</span>#{right}"
  end

end
