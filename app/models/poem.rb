# encoding: UTF-8

class Poem < ActiveRecord::Base
  # attr_accessible :title, :body

  def get_segments
    self.content.mb_chars.split(/，|。|？|\s/).reject! { |s| s.empty? }
  end

  # return the first matched segment that contains the given zi
  def get_matched_segment (zi)
    count = 0
    self.get_segments.each do |s|
      return [s, count] if s.mb_chars.include? zi
      count += 1
    end
    nil
  end

end
