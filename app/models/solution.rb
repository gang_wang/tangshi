class Solution < ActiveRecord::Base
  # attr_accessible :title, :body
  serialize :data

  def is_length_ok?(len)
    self.data.keys.each do |k|
      return false if self.data[k].find_all {|item| item[2] == len }.size == 0
    end
    return true
  end

end
